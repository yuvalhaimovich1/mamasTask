import java.util.Scanner;
public class Store {
	private LilusMain[] tenRec = new LilusMain[10]; // מערך 10 יצורים אחרונים שהוזמנו
	private LilusMain[] creatureStock; // מערך של היצורים הקיימים במלאי (מאגר)
	private LilusMain[] wasMade; // מערך בו נמצאים היצורים שיצרו אותם אך לא נמצאים במאגר


// פעולה בונה לחנות
	public Store(LilusMain[] creatureStock) {
		this.tenRec = null;
		this.creatureStock = creatureStock;
		this.wasMade = null; 
	}
// פעולה בה הלקוח קונה יצור , אם היצור קיים במאגר הוא יינתן ואם לא יווצר חדש, הפעולה מקבלת צבע, גודל היצור, מספר עיניים, האם הוא יכול לעוף ומחזירה את היצור שנקנה
	public LilusMain buyCreature(String colorC,char sizeC, int eyeNumC, boolean canFlyC ) {
		LilusMain creature = new LilusMain(colorC,sizeC,eyeNumC, canFlyC );
		if(colorC != "pink" && colorC != "blue" && colorC && "yellow" &&colorC != "green" && colorC != "red" || sizeC != 's'&&| sizeC != 'm'&& sizeC != 'l') {
			System.out.println("error");
		}
		int i=0;
		while(creatureStock[i]!= creature) {
			i++;	
		}
		if(i<creatureStock.length) {
			System.out.println("we already have your creature in stock :)");
			return creatureStock[i];
			creatureStock[i].setWasAskedNum(wasAskedNum+1);


		}else {
			System.out.println("we made you a brand new creature :)");
			return creature;
			creature.setWasMadeNum(wasMadeNum+1);
			int m=0;
			while(wasMade[m]!= null) {
				m++;
			}
			wasMade[m]=creature;
		}

	}
// פעולה שבה הלקוח מחזיר לחנות את היצור שבבעלותו, הפעולה מקבלת את היצור ומחזירה מערך של המאגר היצורים המעודכן
	public LilusMain[] returnCreature(LilusMain creatureR) {
		int z=0;
		while(creatureStock[z]!= null) {
			z++;
		}
		creatureStock[z]=creatureR;
		return creatureStock;
	}
// פעולה שמציגה את 10 ההזמנות האחרונות, מקבלת מאפייני יצור ומחזירה מערך של 10 ההזמנות האחרונות
	public  LilusMain[] lastOrders(String colorL,char sizeL, int eyeNumL, boolean canFlyL) {
		boolean full = true;
		for(int i=0; i<this.tenRec.length; i++) {
			if(tenRec[i] == null) {
				full= false;
			}
		}
		if(full==false) {
			int s =0;
			while(tenRec[s]!= null) {
				s++;
			}
			tenRec[s]= buyCreature(String colorL,char sizeL, int eyeNumL, boolean canFlyL );

		}else {
			for(int i=0; i<this.tenRec.length-1; i++) {
				tenRec[i]= tenRec[i+1];
			}
			tenRec[tenRec.length]= buyCreature(String colorL,char sizeL, int eyeNumL, boolean canFlyL );
		}

	}
// פעולה אשר בה המנהל יכול לחפש יצורים במאגר על פי צבע, הפעולה מקבלת צבע ומערך ריק ומחזירה מערך של כל היצורים באותו הצבע
	public  LilusMain[] colorInStock(String myColor,LilusMain[] colors) {
		int n=0;
		for(int i=0; i<this.creatureStock.length; i++) {
			if(creatureStock[i].getColor() = myColor) {
				colors[n]= creatureStock[i];
				n++;
			}
		}
		return colors;
	}
// פעולה אשר בודקת האם היצור ''טרנדי'' , מקבלת מספר ויצור ובודקת האם הוא טרנדי , אם כן מחזירה את המערך של המאגר בתוספת היצור הטרנדי
	public LilusMain[] trends( int x,LilusMain creatureN) {
		if(creatureN.getWasMadeNum()> x) {
			int q=0;
			while(creatureStock[q]!= null) {
				q++;
			}
			creatureStock[q]=creatureN;
			return creatureStock;

		}

	}
	//פעולה אשר מציגה למנהל את היצורים הכי מבוקשים, מקבלת מערך ריק ומכניסה אליו את היצורים הכי מבוקשים, ומחזירה את המערך הזה
	public LilusMain[] mostAsked(LilusMain[] popular) {
		int max = creatureStock[0].getWasAskedNum();
		for(int i=1; i<this.creatureStock.length; i++) {
			if(creatureStock[i].getWasAskedNum()> max) {
				max =creatureStock[i].getWasAskedNum();
			}
		}
		for(int i=0; i<this.creatureStock.length; i++) {
			if(creatureStock[i].getWasAskedNum()= max) {
				int w=0;
				while(popular[w]!= null) {
					w++;
				}
				popular[w]=creatureN;
				return popular;

			}
		}
	}
// פעולה אשר בודקת האם המוצר טרנדי על פי כמות פעמים שהוא יוצר ועל פי מאפיינים משותפים, אם המוצר טטרנדי הפעולה מכניסה את היצור למאגר ומחזירה את המאגר המעודכן
	public LilusMain[] trendsWithFeatures( int x,LilusMain creatureNew, int y) {
		if(creatureNew.getWasMadeNum()> x) {
			int num =0;
			for(int i=0; i<this.wasMade.length; i++) {
				if(creatureNew.getColor()== wasMade[i].getColor()) {
					num= num+1;
				}
				if(creatureNew.getSize()== wasMade[i].getSize()) {
					num= num+1;
				}
				if(creatureNew.getEyesNum()== wasMade[i].getEyesNum()) {
					num= num+1;
				}
				if(creatureNew.getCanFly()== wasMade[i].getCanFly()) {
					num= num+1;
				}

			}
			if(num>y) {
				int k=0;
				while(creatureStock[k]!= null) {
					k++;
				}
				creatureStock[k]=creatureNew;
				return creatureStock;
			}
		}

	}



}
