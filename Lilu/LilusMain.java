import java.util.Scanner;
public class LilusMain {
	private String color; //צבע היצור
	private char size; // גודל היצור
	private int eyesNum; //מספר העיינים של היצור
	private boolean canFly;// האם היצור יכול לעוף
	private int wasMadeNum; //כמה פעמים היצור נוצר
	private int wasAskedNum; // כמה פעמים ביקשו אותו

	//פעולה בונה ליצור
	public LilusMain(String color,char size, int eyeNum, boolean canFly) {
		this.color = color;
		this.size = size;
		this.eyesNum = eyesNum;
		this.canFly = canFly;
		this.wasMadeNum= 0;
		this.wasAskedNum=0;
	}

	public String getColor(){
		return this.color;
	}
	public void setColor(String color){
		this.color = color;
	}

	public char getSize(){
		return this.size;
	}
	public void setSize(char size){
		this.size = size;
	}

	public int getEyesNum(){
		return this.eyesNum;
	}
	public void setEyesNum(int eyesNum){
		this.eyesNum = eyesNum;
	}

	public boolean getCanFly(){
		return this.canFly;
	}
	public void setCanFly(boolean canFly){
		this.canFly = canFly;
	}
	public boolean getWasMadeNum(){
		return this.wasMadeNum;
	}
	public void setWasMadeNum(int wasMadeNum){
		this.wasMadeNum = wasMadeNum;
	}
	public boolean getWasAskedNum(){
		return this.wasAskedNum;
	}
	public void setWasAskedNum(int wasAskedNum){
		this.wasAskedNum = wasAskedNum;
	}

	public String toString() {
		return "the features of your creature are : color-" + color + ", size-"+ size+ ", eyes number-"+ eyesNum+", can fly-"+ canFly;
	}
	






}
